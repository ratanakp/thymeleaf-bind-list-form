package com.example.thymeleafbindlistform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeleafBindListFormApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafBindListFormApplication.class, args);
	}
}
