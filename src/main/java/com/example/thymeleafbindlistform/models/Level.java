package com.example.thymeleafbindlistform.models;

import java.util.List;

public class Level {
    private Integer id;
    private String level;
    private List<Subject> subjects;

    public Level() {
    }

    public Level(Integer id, String level, List<Subject> subjects) {
        this.id = id;
        this.level = level;
        this.subjects = subjects;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    @Override
    public String toString() {
        return "Level{" +
                "id=" + id +
                ", level='" + level + '\'' +
                ", subjects=" + subjects +
                '}';
    }
}
