package com.example.thymeleafbindlistform.models;

public class SubjectLevel {
    private Subject subject;
    private Level level;

    public SubjectLevel() {
    }

    public SubjectLevel(Subject subject, Level level) {
        this.subject = subject;
        this.level = level;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "SubjectLevel{" +
                "subject=" + subject +
                ", level=" + level +
                '}';
    }
}
