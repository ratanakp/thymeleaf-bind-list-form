package com.example.thymeleafbindlistform.models;

import java.util.List;

public class User {

    private Integer id;
    private String username;
    private String gender;

    private Address address;

    private List<Role> roles;

    private List<SubjectLevel> subjectLevels;

    public User() {
    }

    public User(Integer id, String username, String gender, Address address, List<Role> roles, List<SubjectLevel> subjectLevels) {
        this.id = id;
        this.username = username;
        this.gender = gender;
        this.address = address;
        this.roles = roles;
        this.subjectLevels = subjectLevels;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public List<SubjectLevel> getSubjectLevels() {
        return subjectLevels;
    }

    public void setSubjectLevels(List<SubjectLevel> subjectLevels) {
        this.subjectLevels = subjectLevels;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", gender='" + gender + '\'' +
                ", address=" + address +
                ", roles=" + roles +
                ", subjectLevels=" + subjectLevels +
                '}';
    }
}