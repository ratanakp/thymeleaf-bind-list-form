package com.example.thymeleafbindlistform.models;

import java.util.List;

public class Subject {

    private Integer id;
    private String subName;
    List<Level> levels;

    public Subject() {
    }

    public Subject(Integer id, String subName, List<Level> levels) {
        this.id = id;
        this.subName = subName;
        this.levels = levels;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public List<Level> getLevels() {
        return levels;
    }

    public void setLevels(List<Level> levels) {
        this.levels = levels;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", subName='" + subName + '\'' +
                ", levels=" + levels +
                '}';
    }
}
