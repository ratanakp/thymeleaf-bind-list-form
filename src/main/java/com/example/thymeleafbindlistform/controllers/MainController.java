package com.example.thymeleafbindlistform.controllers;

import com.example.thymeleafbindlistform.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MainController {

    @GetMapping({"/", "/index", "/home"})
    public String home(Model model) {
        model.addAttribute("user", new User());
        return "index";
    }


    @PostMapping("/add/submit")
    public String save(User user) {

        System.out.println(user);

        return "";
    }


    @GetMapping("/add/sub-level")
    public String showAddSubjectLevelForm() {
        return "add-subject-level";
    }


    @PostMapping("/add/sub-level/submit")
    public String addSubjectLevelSubmit(User user, Model model) {
        System.out.println(user);

        model.addAttribute("user", user);

        return "added-user";
    }
}
